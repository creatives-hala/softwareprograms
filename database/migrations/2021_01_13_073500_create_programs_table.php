<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("name")->nullable();
            $table->text("description")->nullable();
            $table->string("version")->nullable();
            $table->string("size")->nullable();
            $table->string("file")->nullable();
            $table->integer("download_counts")->default('0');
            $table->integer("views")->default('0');
            $table->string("image")->nullable();
            $table->string("core_type")->nullable();
            $table->string("os_system")->nullable();
            $table->boolean("is_new")->default('0');

            $table->bigInteger("category_id")->unsigned()->nullable();
            $table->foreign('category_id')
                  ->references('id')
                  ->on('categories')
                  ->onDelete('cascade');
                  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
