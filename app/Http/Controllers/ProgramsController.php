<?php

namespace App\Http\Controllers;

use App\Program;
use Illuminate\Http\Request;

class ProgramsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programs = Program::all();

        return response()->json(['data'    => $programs,
                                 'message' => 'All programs retrived successfully'], 
                                 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image                 = $request->file('image');
        $imginput['imageName'] = rand().'.'.$image->getClientOriginalExtension();
        $imgdest               = public_path('/upload/images/programs');

        $file                  = $request->file('file');
        $fileinput['fileName'] = rand().'.'.$file->getClientOriginalExtension();
        $filedest              = public_path('/upload/files');

        $program              = new Program;
        $program->name        = $request->name;
        $program->description = $request->description;
        $program->version     = $request->version;
        $program->size        = $request->size;
        $program->core_type   = $request->core_type;
        $program->os_system   = $request->os_system;
        $program->category_id = $request->category_id;
        $program->image       = $imginput['imageName'];
        $program->file        = $fileinput['fileName'];
        $program->save();

        $image->move($imgdest , $imginput['imageName']);
        $file->move($filedest , $fileinput['fileName']);

        return response()->json(['data'    => $program,
                                 'message' => 'program Added successfully'], 
                                  200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $program = Program::find($id);

        if (isset($program)) {
            return response()->json(['data'    => $program,
                                     'message' => 'program retrived successfully'], 
                                      200);
        }
        return response()->json(['data'    => ' ',
                                 'message' => 'Not Found'], 
                                  404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $program = Program::find($id);

        if (isset($program)) {

            if(isset($request->name)){
                $program->name = $request->name;
            }
            if(isset($request->description)){
                $program->description = $request->description;
            }
            if(isset($request->version)){
                $program->version = $request->version;
            }
            if(isset($request->size)){
                $program->size = $request->size;
            }
            if(isset($request->core_type)){
                $program->core_type = $request->core_type;
            }
            if(isset($request->os_system)){
                $program->os_system = $request->os_system;
            }
            if(isset($request->category_id)){
                $program->category_id = $request->category_id;
            }
            if(isset($request->is_new)){
                $program->is_new = $request->is_new;
            }
            if (isset($request->image)) {
                    $image                 = $request->file('image');
                    $imginput['imageName'] = rand().'.'.$image->getClientOriginalExtension();
                    $imgdest               = public_path('/upload/images/programs');
                    $program->image        = $imginput['imgName'];
                    $image->move($imgdest, $imginput['imgName']);
                    
            }
            if (isset($request->file)) {
                    $file                  = $request->file('file');
                    $fileinput['fileName'] = rand().'.'.$file->getClientOriginalExtension();
                    $filedest              = public_path('/upload/files');
                    $program->file         = $fileinput['fileName'];
                    $file->move($filedest, $fileinput['fileName']);
                    
            }
            $program->save();
            return response()->json(['data'    => $program,
                                     'message' => 'program updated successfully'], 
                                      200);
        }
        return response()->json(['data'    => ' ',
                                 'message' => 'Not Found'], 
                                  404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $program = Program::find($id);

        if (isset($program)) {

            $program->delete();

            return response()->json(['data'    => ' ',
                                     'message' => 'program deleted successfully'], 
                                     200);
        }
        return response()->json(['data'    => ' ',
                                 'message' => 'Not Found'], 
                                  404);
    }
}
