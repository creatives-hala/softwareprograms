<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::All();

        return response()->json(['data'    => $categories,
                                 'message' => 'All categories retrived successfully'], 
                                 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $logo              = $request->file('logo');
        $input['logoName'] = rand().'.'.$logo->getClientOriginalExtension();
        $dest              =public_path('/upload/images/categories');

        $category              = new Category;
        $category->name        = $request->name;
        $category->description = $request->description;
        $category->logo        = $input['logoName'];

        $category->save();

        $logo->move($dest , $input['logoName']);

        return response()->json(['data'    => $category,
                                 'message' => 'category Added successfully'], 
                                  200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);

        if (isset($category)) {
            return response()->json(['data'    => $category,
                                     'message' => 'category retrived successfully'], 
                                     200);
        }
        return response()->json(['data'    => ' ',
                                 'message' => 'Not Found'], 
                                  404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        if (isset($category)) {

            if(isset($request->name)){
                $category->name = $request->name;
            }

            if(isset($request->description)){
                $category->description = $request->description;
            }

            if (isset($request->logo)) {
                    $logo              = $request->file('logo');
                    $input['logoName'] = rand().'.'.$logo->getClientOriginalExtension();
                    $dest              = public_path('/upload/images/categories');
                    $category->logo    = $input['logoName'];
                    $logo->move($dest, $input['logoName']);
                    
            }

            $category->save();
            
            return response()->json(['data'    => $category,
                                     'message' => 'category updated successfully'], 
                                     200);
        }
        return response()->json(['data'    => ' ',
                                 'message' => 'Not Found'], 
                                  404);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        if (isset($category)) {

            $category->delete();
            return response()->json(['data'    => ' ',
                                     'message' => 'category deleted successfully'], 
                                     200);
        }
        return response()->json(['data'    => ' ',
                                 'message' => 'Not Found'], 
                                  404);
    }
}
