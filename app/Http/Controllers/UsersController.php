<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return response()->json(['data'    => $users,
                                 'message' => 'All users retrived successfully'], 
                                 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user           = new User;
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json(['data'    => $user,
                                 'message' => 'user Added successfully'], 
                                  200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if (isset($user)) {
            return response()->json(['data'    => $user,
                                     'message' => 'user retrived successfully'], 
                                     200);
        }
        return response()->json(['data'    => ' ',
                                 'message' => 'Not Found'], 
                                  404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (isset($user)) {

            if(isset($request->name)){
                $user->name = $request->name;
            }

            if(isset($request->email)){
                $user->email = $request->email;
            }

            if(isset($request->password)){
                $user->password = bcrypt($request->password);
            }
           
            $user->save();
            
            return response()->json(['data'    => $user,
                                     'message' => 'User updated successfully'], 
                                     200);
        }
        return response()->json(['data'    => ' ',
                                 'message' => 'Not Found'], 
                                  404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (isset($user)) {

            $user->delete();
            return response()->json(['data'    => ' ',
                                     'message' => 'user deleted successfully'], 
                                     200);
        }
        return response()->json(['data'    => ' ',
                                 'message' => 'Not Found'], 
                                  404);
    }
}
