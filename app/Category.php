<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'description',
        'logo'
    ];
     public function programs(){
  		return $this->hasMany(Program::Class);
    }
}
