<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $fillable = [
        'name',
        'description',
        'version',
        'size',
        'file',
        'download_counts',
        'views',
        'image',
        'core_type',
        'os_system',
        'is_new',
        'category_id'
    ];
    public function category()
    {
    	return $this->belongsTo(Category::Class);
    }
}
